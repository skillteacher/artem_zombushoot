using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Ammo : MonoBehaviour
{
    [SerializeField] private int ammoAmount = 10;
    [SerializeField] private Text ammoText;


    private void Start()
    {
        ammoText.text = ammoAmount.ToString();
    }

    public void AddAmmo(int amount) 
    {
        ammoAmount += amount;
        ammoText.text = ammoAmount.ToString();
    }

    public void ReduceAmmoByOne() 
    {
        ammoAmount--;
    ammoText.text = ammoAmount.ToString();
    }

    public bool IsAmmoEnough() 
    {
        return ammoAmount > 0;
    }
}
