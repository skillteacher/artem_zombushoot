using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(NavMeshAgent))]
public class EnemyAI : MonoBehaviour
{
    [SerializeField] private Transform target;
    [SerializeField] private float viewRange = 10f;
    [SerializeField] private float damage = 40f;
    [SerializeField] private Animator animator;
    [SerializeField] private EnemyAnimation enemyAnimation;
    private NavMeshAgent navMeshAgent;
    private bool isProvoked;
    private PlayerHealth playerHealth;

    private void Awake()
    {
        navMeshAgent = GetComponent<NavMeshAgent>();
        playerHealth = target.GetComponent<PlayerHealth>();
    }

    private void Update()
    {
        float distanceToTarget = Vector3.Distance(target.position, transform.position);

        EventTracking(distanceToTarget);

        if (isProvoked)
        {
            EngageTarget(distanceToTarget);
        }

    }

    private void EventTracking(float distance)
    {
        if (distance <= viewRange)
        {
            isProvoked = true;
            animator.SetBool("move", true);
        }
    }

    private void EngageTarget(float distance)
    {
        if (distance > navMeshAgent.stoppingDistance)
        {
            Chase();
        }
        else
        {
            Attack();
        }
    }

    private void Chase()
    {
        navMeshAgent.SetDestination(target.position);
        animator.SetBool("attack", false);
    }

    private void Attack()
    {
        navMeshAgent.SetDestination(target.position);
        animator.SetBool("attack", true);
    }

    private void OnDrawGizmos()
    {
        Gizmos.DrawWireSphere(transform.position, viewRange);
    }

    private void OnEnable()
    {
        enemyAnimation.AnimationAttackMoment += TargetAttack;
    }

    private void OnDisable()
    {
        enemyAnimation.AnimationAttackMoment -= TargetAttack;
    }
    public void TargetAttack()
    {
        playerHealth.TakeDamage(damage);
    }

}

