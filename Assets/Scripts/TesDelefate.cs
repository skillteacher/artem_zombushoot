using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TesDelefate : MonoBehaviour
{
    delegate void WriteDelegate();
    private WriteDelegate varDelegate;

    event WriteDelegate WriteEvent;

    public bool isStartEvent = false;
    private void OnEnable()
    {
        WriteEvent += WriteToLog;
    }
    private void Update()
    {
        if (isStartEvent) 
        {
            WriteEvent?.Invoke();
            isStartEvent = false;
        }
    }
    private void OnDisable()
    {
        WriteEvent -= WriteToLog;
    }

    private void Start()
    {
        //varDelegate();
    }
    public void WriteToLog() 
    {
        Debug.Log("Write!");
    }

    public void Write2() 
    {
        Debug.Log("2");
    }

  
}


