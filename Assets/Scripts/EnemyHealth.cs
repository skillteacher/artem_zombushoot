using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHealth : MonoBehaviour
{
   [SerializeField] private float HitPoints = 100f;

    public void TakeDamage(float damangeAmount) 
    {
        HitPoints -= damangeAmount;
        if (HitPoints <= 0) 
        {
            Destroy(gameObject);
        }
    }
}
