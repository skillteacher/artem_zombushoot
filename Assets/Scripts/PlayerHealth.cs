using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHealth : MonoBehaviour
{ 
    public event Message PlayerDeath;

    [SerializeField] private float hitPoints = 100f;

    public void TakeDamage(float damageAmount) 
    {
        hitPoints -= damageAmount;
        if (hitPoints<=0) 
        {
            Debug.Log("YOU DIED!");
            PlayerDeath?.Invoke();
        }
    }
}
