using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon : MonoBehaviour
{
    [SerializeField] private float range = 100f;
    [SerializeField] private Transform cameraTranform;
    [SerializeField] private float damage = 10f;
    [SerializeField] private ParticleSystem muzzleFlashEffect;
    [SerializeField] private GameObject sparksPrefab;
    [SerializeField] private float sparksLifetime = 0.1f;
    [SerializeField] private Ammo ammo; 

    private void Update()
    {
        if (Input.GetButtonDown("Fire1")) 
        {
            Fire();
        }
    }

    
     private void Fire()
    {
        if (!ammo.IsAmmoEnough()) return;
        ammo.ReduceAmmoByOne();
        PlayMuzzleEffect();
        Raycasting();

    }

    private void PlayMuzzleEffect()
    {
        muzzleFlashEffect.Play();
    }

    private void Raycasting()
    {
        RaycastHit hit;
        if (!Physics.Raycast(cameraTranform.position, cameraTranform.forward, out hit, range)) return;
        HitEffect(hit.point);
        EnemyHealth enemyhealth = hit.transform.GetComponent<EnemyHealth>();
        if (enemyhealth)
        {
            enemyhealth.TakeDamage(damage);
        }
    }


    private void HitEffect(Vector3 point)
    {
        GameObject sparks = Instantiate(sparksPrefab, point, Quaternion.identity);
        Destroy(sparks, sparksLifetime);
    }
}



       