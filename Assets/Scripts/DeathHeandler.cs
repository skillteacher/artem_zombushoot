using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeathHeandler : MonoBehaviour
{
    [SerializeField] private Canvas gameOverCanvas;
    [SerializeField] private PlayerHealth playerHealth;

    private void Start()
    {
        gameOverCanvas.enabled = false;
        Time.timeScale = 1f;
    }

    private void OnEnable()
    {
        playerHealth.PlayerDeath += OnPlayerDeath;
    }

    private void OnDisable()
    {
        playerHealth.PlayerDeath -= OnPlayerDeath;
    }

    private void OnPlayerDeath()
    {
        gameOverCanvas.enabled = true;
        Time.timeScale = 0f;
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;

    }
}