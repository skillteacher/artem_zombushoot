using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(NavMeshAgent))]
public class EnemyMovement : MonoBehaviour
{
    [SerializeField] private Transform target;
    [SerializeField] private float viewRange = 10f;
    [SerializeField] private Animator animator;
    private NavMeshAgent navMeshAgent;
    private bool isProvoked; 

    private void Awake()
    {
        navMeshAgent = GetComponent<NavMeshAgent>();
    }

    private void Update()
    {
        float distanceToTarget = Vector3.Distance(target.position, transform.position);

        EventTracking(distanceToTarget);

        if(isProvoked)
        {
            EngageTarget(distanceToTarget);
        }
        
    }

    private void EventTracking(float distance)
    {
        if  (distance <= viewRange)
        {
            isProvoked = true;
            animator.SetBool("move", true);
        }
    }

    private void EngageTarget(float distance)
    {
        if (distance > navMeshAgent.stoppingDistance)
        {
            Chase();
        }
        else 
        {
            Attack();
        }
    }

    private void Chase() 
    {
        navMeshAgent.SetDestination(target.position);
        animator.SetBool("attack", false);
    }

    private void Attack() 
    {
        navMeshAgent.SetDestination(target.position);
        animator.SetBool("attack", true);
    }

    private void OnDrawGizmos()
    {
        Gizmos.DrawWireSphere(transform.position, viewRange);
    }
}

